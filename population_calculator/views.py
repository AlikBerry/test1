from django.shortcuts import render


def index(request):
    params ={
        "result": "",
        "error_message": "",
        "year": request.POST.get("year") 
    }

    import math

    increase_per_second = 1 / 8 - 1 / 12 + 1 / 29

    if request.POST.get("calculate"):
        try:
            year = int(params['year'])
            params['result'] = math.ceil(increase_per_second * ((year -2018) *60 * 60 * 24 * 365))
        except:
            params["error_message"] ="type correct year"

    return render(request, "population_calculator/index.html", params)