from django.apps import AppConfig


class PopulationCalculatorConfig(AppConfig):
    name = 'population_calculator'
